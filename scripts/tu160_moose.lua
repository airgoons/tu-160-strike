USSRBorderZone = ZONE_POLYGON:New( "USSR", GROUP:FindByName( "USSR" ) )
GeorgiaBorderZone = ZONE_POLYGON:New( "Georgia", GROUP:FindByName( "GEORGIA" ) )


--OUR LOCAL PLAYERS:
--Transcaucasus Military District, AKA Georgia
  --283rd Fighter Aviation Division
    --982nd Fighter Aviation Regiment (BLUEFOR, Stationed near Tbilisi) - Inventory circa 1990, 40 MiG-23
    --176th Fighter Aviation Regiment - Inventory circa 1990 - 35 MiG-29, 4 MiG-23
-- Moscow Military District (we'll say they flew down to reinforce the line).
  --9th Fighter Aviation Division
    --234th Guards Mixed Aviation Regiment - Inventory circa 1990: 16 Su-27, 15 MiG-29, 5 Su-24, 6 Su-25 and 3 Su-22(Su-17)
    --274th Fighter-Bomber Aviation Regiment - Inventory circa 1990: 40 Su-17M4

--These are pre-spawned
--MAR234SU27=SQUADRON:New("SU27", 2, "234th Guards - Su-27s")
--MAR234SU27:SetCallsign(CALLSIGN.Aircraft.Dodge)
--MAR234SU27:SetSkill(AI.Skill.GOOD)
--MAR234SU27:AddMissionCapability({AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT}, 90)
--MAR234SU27:AddMissionCapability({AUFTRAG.Type.INTERCEPT}, 70)

MAR234MIG29=SQUADRON:New("MIG29", 6, "234th Guards - MiG-29")
MAR234MIG29:SetCallsign(CALLSIGN.Aircraft.Uzi)
MAR234MIG29:SetSkill(AI.Skill.AVERAGE)
MAR234MIG29:AddMissionCapability({AUFTRAG.Type.INTERCEPT, AUFTRAG.Type.CAP , AUFTRAG.Type.ESCORT}, 80)
MAR234MIG29:SetEngagementRange(120)

MAR234SU24=SQUADRON:New("SU24", 1, "234th Guards - Su-24")
MAR234SU24:SetCallsign(CALLSIGN.Aircraft.Enfield)
MAR234SU24:SetSkill(AI.Skill.GOOD)
MAR234SU24:AddMissionCapability({ AUFTRAG.Type.SEAD, AUFTRAG.Type.BAI }, 80)
MAR234SU24:SetEngagementRange(160)

MAR234SU25=SQUADRON:New("SU25", 1, "234th Guards - Su-25")
MAR234SU25:SetCallsign(CALLSIGN.Aircraft.Chevy)
MAR234SU25:SetSkill(AI.Skill.AVERAGE)
MAR234SU25:AddMissionCapability({ AUFTRAG.Type.CAP, AUFTRAG.Type.BAI }, 90)
MAR234SU25:SetEngagementRange(160)

FBR274SU17=SQUADRON:New("SU17M4", 8, "274th FBA - Su-17")
FBR274SU17:SetCallsign(CALLSIGN.Aircraft.Enfield)
FBR274SU17:AddMissionCapability({ AUFTRAG.Type.SEAD, AUFTRAG.Type.BAI, AUFTRAG.Type.CAS }, 60)
FBR274SU17:SetEngagementRange(240)

FAR176MIG29=SQUADRON:New("MIG29", 15, "176th FAR - MiG-29")
FAR176MIG29:SetCallsign(CALLSIGN.Aircraft.Uzi)
FAR176MIG29:SetSkill(AI.Skill.AVERAGE)
FAR176MIG29:AddMissionCapability({AUFTRAG.Type.INTERCEPT, AUFTRAG.Type.CAP , AUFTRAG.Type.ESCORT}, 80)
FAR176MIG29:SetEngagementRange(120)


-- Create TWO AIRWINGS - note these are warehouses and in the case of ships, the ship name
MAY = AIRWING:New("MAY Warehouse", "234th Guards") --Ops.AirWing#AIRWING
KRA = AIRWING:New("KRA Warehouse", "274th FBA")
ANA = AIRWING:New("ANA Warehouse", "176th FAR")


  -- Add squadrons to airwing.
--MAY:AddSquadron(MAR234SU27)
MAY:AddSquadron(MAR234MIG29)
MAY:AddSquadron(MAR234SU24)
MAY:AddSquadron(MAR234SU25)
KRA:AddSquadron(FBR274SU17)
ANA:AddSquadron(FAR176MIG29)

--PAYLOADS (multiples can be made)
--local SU27Intercept=MAY:NewPayload(GROUP:FindByName("SU27_INTC"), -1 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT}, 80)
local MiG29A2A=MAY:NewPayload(GROUP:FindByName("MIG29_A2A"), -1 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT}, 50)
local MiG29CAS=MAY:NewPayload(GROUP:FindByName("MIG29_CAS"), -1 ,{AUFTRAG.Type.CAS, AUFTRAG.Type.INTERCEPT, AUFTRAG.Type.CAP}, 40)
local MiG29A2A2=ANA:NewPayload(GROUP:FindByName("MIG29_A2A"), -1 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT}, 50)
local MiG29CAS2=ANA:NewPayload(GROUP:FindByName("MIG29_CAS"), -1 ,{AUFTRAG.Type.CAS, AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP }, 40)
local SU24SEAD=MAY:NewPayload(GROUP:FindByName("SU24_SEAD"), -1 ,{AUFTRAG.Type.SEAD}, 60)
local SU24BAI=MAY:NewPayload(GROUP:FindByName("SU24_BAI"), -1 ,{AUFTRAG.Type.BAI}, 50)
local SU25CAS=MAY:NewPayload(GROUP:FindByName("SU25_CAS"), -1 ,{AUFTRAG.Type.CAS}, 60)
local SU25BAI=MAY:NewPayload(GROUP:FindByName("SU25_BAI"), -1 ,{AUFTRAG.Type.BAI}, 50)
local SU17SEAD=KRA:NewPayload(GROUP:FindByName("SU17M4_SEAD"), 10 ,{AUFTRAG.Type.SEAD}, 50)
local SU17CAS=KRA:NewPayload(GROUP:FindByName("SU17M4_CAS"), -1 ,{AUFTRAG.Type.CAS}, 50)
local SU17BAI=KRA:NewPayload(GROUP:FindByName("SU17M4_BAI"), -1 ,{AUFTRAG.Type.BAI}, 50)
local SU17CAS1=KRA:NewPayload(GROUP:FindByName("SU17M4_CAS1"), -1 ,{AUFTRAG.Type.CAS}, 50)
local SU17BAI1=KRA:NewPayload(GROUP:FindByName("SU17M4_BAI1"), -1 ,{AUFTRAG.Type.BAI}, 50)

--raise cap limit DOES NOT WORK DONT DO THIS HORRIBLE THINGS WILL HAPPEN IF YOU DO THIS
--MAY:SetNumberCAP(9)
--MIN:SetNumberCAP(9)
--KRA:SetNumberCAP(9)
--NOV:SetNumberCAP(9)
--Start the Airwings
MAY:SetMarker(false)
KRA:SetMarker(false)
ANA:SetMarker(false)
MAY:SetReportOff()
KRA:SetReportOff()
ANA:SetReportOff()
MAY:Start()
KRA:Start()
ANA:Start()
-- Create a SET of all Blue Groups

ZoneCAP = ZONE:New("CAP1") 
missionCAP=AUFTRAG:NewCAP(ZoneCAP, 25000, 350)
ZoneCAP2 = ZONE:New("CAP2")
missionCAP2=AUFTRAG:NewCAP(ZoneCAP2, 25000, 350)
ussrcapgroup1=FLIGHTGROUP:New("CAP1-SU27-USSR")
ussrcapgroup2=FLIGHTGROUP:New("CAP2-SU27-USSR")
ussrcapgroup1:AddMission(missionCAP)
ussrcapgroup2:AddMission(missionCAP2)


local AgentSet=SET_GROUP:New():FilterCoalitions("red"):FilterStart()

local scouts=SET_GROUP:New():FilterPrefixes("scout"):FilterCoalitions("red"):FilterStart()

--Declare an INTEL based off that SET, for BLUE coalition
local KGB=INTEL:New(AgentSet, coalition.side.red)  
KGB:AddAcceptZone(USSRBorderZone) --give it an inclusive area  
KGB:Start() --start it

local CASSEAD=INTEL:New(scouts, coalition.side.red)  
CASSEAD:AddAcceptZone(GeorgiaBorderZone) --give it an inclusive area  
CASSEAD:SetForgetTime(4800)
CASSEAD:Start() --start it

--- Function called when a NEW contact is detected.
function KGB:OnAfterNewContact(From, Event, To, Contact)
  local contact=Contact --Ops.Intelligence#INTEL.Contact
  ATO(contact, {MAY,KRA,ANA}) --fire the function for the contact  
end --end OnAfterNew

function groundfire(contact) 
  local auftrag
  if (contact.attribute == "Ground_SAM") or (contact.attribute == "Ground_AAA") then
    local target=GROUP:FindByName(contact.groupname)
    auftrag=AUFTRAG:NewSEAD(target, 22500)
    auftrag:SetMissionAltitude(15000)
    auftrag:SetEngageAltitude(15000)
    auftrag:SetEngageAsGroup(false)
    auftrag:SetRepeatOnFailure(25)


  elseif contact.attribute == "Ground_Tank" then
   local grp= GROUP:FindByName(contact.groupname)
   local vec2 = grp:GetVec2()
   local coord = grp:GetCoordinate()
   local zoneR = ZONE_RADIUS:New("CasZone", vec2, 10000)
   auftrag=AUFTRAG:NewCAS(zoneR, 10000, 300, coord, 270, 10)
  elseif (contact.attribute == "Ground_APC") or (contact.attribute == "Ground_Artillery") or (contact.attribute =="Ground_Truck") or (contact.attribute =="Ground_EWR") then
    local target=GROUP:FindByName(contact.groupname)
    local auftrag=AUFTRAG:NewBAI(target, 1000)
    auftrag:SetMissionAltitude(1000)
  end
  if auftrag ~= nil then
    function auftrag:OnAfterStarted(From,Event,To)
      for _,flightgroup in pairs(auftrag:GetOpsGroups()) do
        local escort=AUFTRAG:NewESCORT(flightgroup:GetGroup())
          MAY:AddMission(escort)
          ANA:AddMission(escort)
      end
    end
    KRA:AddMission(auftrag)
    MAY:AddMission(auftrag)
    ANA:AddMission(auftrag)
  end
end --endfunc

--- Function called when a NEW contact is detected.
function CASSEAD:OnAfterNewContact(From, Event, To, Contact)
  local contact=Contact --Ops.Intelligence#INTEL.Contact
  groundfire(contact) --fire the function for the contact  
end --end OnAfterNew


--- Bluefor
FAR982MIG23=SQUADRON:New("MIG23", 10, "982nd FAR - MiG-23")
FAR982MIG23:SetCallsign(CALLSIGN.Aircraft.Uzi)
FAR982MIG23:SetSkill(AI.Skill.AVERAGE)
FAR982MIG23:AddMissionCapability({AUFTRAG.Type.INTERCEPT, AUFTRAG.Type.CAP , AUFTRAG.Type.ESCORT}, 50)
FAR982MIG23:SetEngagementRange(300)

SUK = AIRWING:New("SUK Warehouse", "982nd FAR")
SUK:AddSquadron(FAR982MIG23)
local MiG23A2A=SUK:NewPayload(GROUP:FindByName("MIG23_A2A"), -1 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT}, 50)

SUK:SetMarker(false)
SUK:SetReportOff()
SUK:Start()

georgiacapgroup=FLIGHTGROUP:New("CAP-MiG23-Georgia")
georgiaZoneCAP = ZONE:New("Georgia CAP 1") 
georgiacap=AUFTRAG:NewCAP(georgiaZoneCAP, 25000, 350)
SUK:AddMission(georgiacap)

local BAgentSet=SET_GROUP:New():FilterCoalitions("blue"):FilterStart()
local BlueIntel=INTEL:New(BAgentSet, coalition.side.blue)  
BlueIntel:AddAcceptZone(GeorgiaBorderZone) --give it an inclusive area  

--- Function called when a NEW contact is detected.
function BlueIntel:OnAfterNewContact(From, Event, To, Contact)
  local contact=Contact --Ops.Intelligence#INTEL.Contact
  ATO(contact, {SUK}) --fire the function for the contact
end --end OnAfterNew
BlueIntel:Start() --start it





--THIS IS A CUSTOM FUNCTION TO CREATE A MISSION FOR ALL DETECTIONS OF THE RIGHT TYPE
function ATO(contact, airwings)
  local auftrag 
  if contact.attribute == "Air_Bomber" then
      local grp= GROUP:FindByName(contact.groupname)
      local vec2 = grp:GetVec2()
      local coord = grp:GetCoordinate()
      local zoneR = ZONE_RADIUS:New("CapZone", vec2, 90000)
      auftrag=AUFTRAG:NewCAP(zoneR, 20000, 500, coord, 2700, 10)
      auftrag:SetRepeatOnFailure(5)
      --MAY:AddMission(auftrag)
      --ANA:AddMission(auftrag)
  
  elseif contact.attribute == "Air_Fighter" then
    local grp= GROUP:FindByName(contact.groupname)
    auftrag=AUFTRAG:NewINTERCEPT(grp)
    auftrag:SetRepeatOnFailure(5)
    auftrag:SetPriority(70, true) -- true here sets the task to cancel lower priority tasks in favor this mission
    --MAY:AddMission(intercept)
    --ANA:AddMission(intercept)
  end
  
  if auftrag ~= nil then
    for _, airwing in pairs(airwings) do
      airwing:AddMission(auftrag)
    end
  end

end --endfunc

--[[
function CAP1()
  local target=GROUP:FindByName("Avenger Flight")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  MAY:AddMission(auftrag)
  ANA:AddMission(auftrag)
end

function CAP2()
  local target=GROUP:FindByName("Bearcat Flight")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  MAY:AddMission(auftrag)
  ANA:AddMission(auftrag)
end

function CAP3()
  local target=GROUP:FindByName("Buffalo Flight")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  MAY:AddMission(auftrag)
  ANA:AddMission(auftrag)
end

function CAP4()
  local target=GROUP:FindByName("Havoc Flight")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  MAY:AddMission(auftrag)
  ANA:AddMission(auftrag)
end

function CAP5()
  local target=GROUP:FindByName("Hellcat Flight")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  MAY:AddMission(auftrag)
  ANA:AddMission(auftrag)
end

function CAP6()
  local target=GROUP:FindByName("Tigercat Flight")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  MAY:AddMission(auftrag)
  ANA:AddMission(auftrag)
end

function CAP7()
  local target=GROUP:FindByName("Wildcat Flight")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  MAY:AddMission(auftrag)
  ANA:AddMission(auftrag)
end

function CAP8()
  local target=GROUP:FindByName("Harriers *Lassie*")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  MAY:AddMission(auftrag)
  ANA:AddMission(auftrag)
end


function CAP9()
  local target=GROUP:FindByName("Eagles *Pavlov*")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  MAY:AddMission(auftrag)
  ANA:AddMission(auftrag)
end




--Command=SET_GROUP:New():FilterPrefixes("mis"):FilterCoalitions("blue"):FilterStart()
Command=GROUP:FindByName("1 mission commander")

--TOP MENU
TopMenu1 = MENU_GROUP:New(Command,"Spawn Some enemies" )
-- TopMenu3 = MENU_GROUP:New( "Auftrag Demo 17>>" )
--FIRST SUB MENU
Menu1 = MENU_GROUP_COMMAND:New(Command,"Avenger Flight", TopMenu1, CAP1)
Menu2 = MENU_GROUP_COMMAND:New(Command,"Bearcat Flight", TopMenu1, CAP2)
Menu3 = MENU_GROUP_COMMAND:New(Command,"Buffalo Flight", TopMenu1, CAP3)
Menu4 = MENU_GROUP_COMMAND:New(Command,"Havoc Flight", TopMenu1, CAP4)

Menu5 = MENU_GROUP_COMMAND:New(Command,"Hellcat Flight", TopMenu1, CAP5)
Menu6 = MENU_GROUP_COMMAND:New(Command,"Tigercat Flight", TopMenu1, CAP6)
Menu7 = MENU_GROUP_COMMAND:New(Command,"Wildcat Flight", TopMenu1, CAP7)
--Menu8 = MENU_GROUP_COMMAND:New(Command,"Harriers *Lassie*", TopMenu1, CAP8)

--Menu9 = MENU_GROUP_COMMAND:New(Command,"Eagles *Pavlov*", TopMenu1, CAP9)
--]]
