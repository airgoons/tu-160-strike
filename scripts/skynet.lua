do
--create an instance of the IADS
ussrIADS = SkynetIADS:create('USSR')

---debug settings remove from here on if you do not wan't any output on what the IADS is doing by default
local iadsDebug = ussrIADS:getDebugSettings()
iadsDebug.IADSStatus = false
iadsDebug.samWentDark = false
iadsDebug.contacts = false
iadsDebug.radarWentLive = false
iadsDebug.noWorkingCommmandCenter = false
iadsDebug.ewRadarNoConnection = false
iadsDebug.samNoConnection = false
iadsDebug.jammerProbability = false
iadsDebug.addedEWRadar = false
iadsDebug.hasNoPower = false
iadsDebug.harmDefence = false
iadsDebug.samSiteStatusEnvOutput = false
iadsDebug.earlyWarningRadarStatusEnvOutput = false
---end remove debug ---

ussrIADS:addEarlyWarningRadarsByPrefix('EWR-USSR')
ussrIADS:addSAMSitesByPrefix('SAM-USSR')

ussrIADS:getSAMSitesByPrefix('SAM-USSR-SA2'):setActAsEW(true)
ussrIADS:getSAMSitesByPrefix('SAM-USSR-SA10'):setActAsEW(true)

ussrIADS:getSAMSitesByPrefix('SAM-USSR-SA2'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):setGoLiveRangeInPercent(110)
ussrIADS:getSAMSitesByPrefix('SAM-USSR-SA3'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):setGoLiveRangeInPercent(110)
ussrIADS:getSAMSitesByPrefix('SAM-USSR-SA8'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):setGoLiveRangeInPercent(110)
ussrIADS:getSAMSitesByPrefix('SAM-USSR-SA11'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):setGoLiveRangeInPercent(105)

ussrIADS:setupSAMSitesAndThenActivate()

georgiaIADS = SkynetIADS:create('GEORGIA')
---debug settings remove from here on if you do not wan't any output on what the IADS is doing by default
local iadsDebug = georgiaIADS:getDebugSettings()
iadsDebug.IADSStatus = false
iadsDebug.samWentDark = false
iadsDebug.contacts = false
iadsDebug.radarWentLive = false
iadsDebug.noWorkingCommmandCenter = false
iadsDebug.ewRadarNoConnection = false
iadsDebug.samNoConnection = false
iadsDebug.jammerProbability = false
iadsDebug.addedEWRadar = false
iadsDebug.hasNoPower = false
iadsDebug.harmDefence = false
iadsDebug.samSiteStatusEnvOutput = false
iadsDebug.earlyWarningRadarStatusEnvOutput = false
---end remove debug ---

georgiaIADS:addEarlyWarningRadarsByPrefix('EWR-GEORGIA')
georgiaIADS:addSAMSitesByPrefix('SAM-GEORGIA')

ussrIADS:getSAMSitesByPrefix('SAM-USSR-SA2'):setActAsEW(true)
georgiaIADS:getSAMSitesByPrefix('SAM-GEORGIA-SA10'):setActAsEW(true)

--I'm not going to put too much effort into Georgia for this one.  Maybe configure the SA-15 PD if Dark Off has the USSR initiate its own SEAD.
georgiaIADS:setupSAMSitesAndThenActivate()

end