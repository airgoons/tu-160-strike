--[[function Timer_for_setup()
  MessageToAll('radar has warmed up')
end
]]

function Backup_for_power()
  local backup = StaticObject.getByName('backup_power')
  MessageToAll('backup')
  hawk:addPowerSource(backup)
end


--create an instance of the IADS
iranIADS = SkynetIADS:create('Iran')

---debug settings remove from here on if you do not wan't any output on what the IADS is doing by default
local iadsDebug = iranIADS:getDebugSettings()
iadsDebug.IADSStatus = false
iadsDebug.samWentDark = false
iadsDebug.contacts = false
iadsDebug.radarWentLive = false
iadsDebug.noWorkingCommmandCenter = false
iadsDebug.ewRadarNoConnection = false
iadsDebug.samNoConnection = false
iadsDebug.jammerProbability = false
iadsDebug.addedEWRadar = false
iadsDebug.hasNoPower = false
iadsDebug.harmDefence = false
iadsDebug.samSiteStatusEnvOutput = false
iadsDebug.earlyWarningRadarStatusEnvOutput = false
---end remove debug ---

--add all units with unit name beginning with 'EW' to the IADS:
iranIADS:addEarlyWarningRadarsByPrefix('EW')
iranIADS:addSAMSitesByPrefix('sam')
--iranIADS:getSAMSitesByPrefix('hawk'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_SEARCH_RANGE):setGoLiveRangeInPercent(50)
--add all groups begining with group name 'SAM' to the IADS:

--local cc2 = Unit.getByName('CC-IRAN Qeshm')
--iranIADS:addCommandCenter(cc2)

local power = StaticObject.getByName('power')
--Set golive so that aircraft are generally in kill zone by the time we can get a tracking solution (hopefully!).


--iranIADS:getSAMSitesByPrefix('SAM-IRAN-Hawk'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE)
hawk = iranIADS:getSAMSitesByNatoName('Hawk'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):setGoLiveRangeInPercent(110):addPowerSource(power)
--hawk = iranIADS:getSAMSitesByPrefix('hawk')
--hawk:setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):addPowerSource(power)
iranIADS:getSAMSitesByNatoName('SA-10'):setActAsEW(true):setHARMDetectionChance(70)
iranIADS:getSAMSitesByNatoName('SA-2'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):setGoLiveRangeInPercent(75):setHARMDetectionChance(60)
iranIADS:getSAMSitesByNatoName('SA-11'):setEngagementZone(SkynetIADSAbstractRadarElement.GO_LIVE_WHEN_IN_KILL_ZONE):setGoLiveRangeInPercent(125)


-- activate the IADS
iranIADS:addRadioMenu()
iranIADS:activate()

IranBorderZone = ZONE_POLYGON:New( "iran", GROUP:FindByName( "iran" ) )

-- VVVV BACKUP VVVV 
--[[
-- Define a SET_GROUP object that builds a collection of groups that define the EWR network.
DetectionSetGroup = SET_GROUP:New()

-- add the MOOSE SET_GROUP to the Skynet IADS, from now on Skynet will update active radars that the MOOSE SET_GROUP can use for EW detection.
iranIADS:addMooseSetGroup(DetectionSetGroup)

-- Setup the detection and group targets to a 50km range!
Detection = DETECTION_AREAS:New( DetectionSetGroup, 50000 )

-- Setup the A2A dispatcher, and initialize it.
A2ADispatcher = AI_A2A_DISPATCHER:New( Detection )

-- Set 100km as the radius to engage any target by airborne friendlies.
A2ADispatcher:SetEngageRadius() -- 100000 is the default value.

-- Set 200km as the radius to ground control intercept.
A2ADispatcher:SetGciRadius() -- 200000 is the default value.

A2ADispatcher:SetBorderZone( CCCPBorderZone )

A2ADispatcher:SetSquadron( "Bandar", AIRBASE.PersianGulf.Bandar_Abbas_Intl, { "a2a bandar" }, 6 )
A2ADispatcher:SetSquadronGrouping( "Bandar", 2 )
A2ADispatcher:SetSquadronGci( "Bandar", 900, 1200 )

A2ADispatcher:SetSquadron( "Qeshm", AIRBASE.PersianGulf.Qeshm_Island , { "a2a Qeshm" }, 9 )
A2ADispatcher:SetSquadronGrouping( "Qeshm", 2 )
A2ADispatcher:SetSquadronGci( "Qeshm", 900, 1200 )

A2ADispatcher:SetTacticalDisplay(false)
A2ADispatcher:Start()

--]]

--CREATE TWO SQUADRONS
F4=SQUADRON:New("f4", 4, "f-44444") --Ops.Squadron#SQUADRON
F4:SetCallsign(CALLSIGN.Aircraft.Dodge)
F4:SetSkill(AI.Skill.EXCELLENT)
F4:AddMissionCapability({ AUFTRAG.Type.INTERCEPT, AUFTRAG.Type.CAP , AUFTRAG.Type.ESCORT }, 70)
F4:SetEngagementRange(200)

MIG29=SQUADRON:New("mig29", 4, "mig-22222") --Ops.Squadron#SQUADRON 
MIG29:SetCallsign(CALLSIGN.Aircraft.Uzi)
MIG29:SetSkill(AI.Skill.EXCELLENT)
MIG29:AddMissionCapability({AUFTRAG.Type.INTERCEPT, AUFTRAG.Type.CAP,AUFTRAG.Type.ESCORT }, 99) --AUFTRAG.Type.FACA AUFTRAG.Type.CAS AUFTRAG.Type.BOMBING
MIG29:SetEngagementRange(240)

F5=SQUADRON:New("f5", 7, "f-5555") --Ops.Squadron#SQUADRON
F5:SetCallsign(CALLSIGN.Aircraft.Chevy)
F5:SetSkill(AI.Skill.EXCELLENT)
F5:AddMissionCapability({ AUFTRAG.Type.INTERCEPT, AUFTRAG.Type.CAP , AUFTRAG.Type.ESCORT }, 80)
F5:SetEngagementRange(200)

F4E=SQUADRON:New("f44", 4, "f-477777E") --Ops.Squadron#SQUADRON
F4E:SetCallsign(CALLSIGN.Aircraft.Enfield)
F4E:SetSkill(AI.Skill.EXCELLENT)
F4E:AddMissionCapability({ AUFTRAG.Type.INTERCEPT, AUFTRAG.Type.CAP , AUFTRAG.Type.ESCORT }, 70)
F4E:SetEngagementRange(200)


-- Create TWO AIRWINGS - note these are warehouses and in the case of ships, the ship name
 LAN = AIRWING:New("LAN Warehouse", "LAN Warehouse") --Ops.AirWing#AIRWING
 BAN = AIRWING:New("BAN Warehouse", "BAN Warehouse")
 LAV = AIRWING:New("LAV Warehouse", "LAV Warehouse") --Ops.AirWing#AIRWING
 LAR = AIRWING:New("LAR Warehouse", "LAR Warehouse")

  -- Add squadrons to airwing.
LAN:AddSquadron(F4)
LAV:AddSquadron(MIG29)
BAN:AddSquadron(F5)
LAR:AddSquadron(F4E)



--PAYLOADS (multiples can be made)
local f4Intercept=LAN:NewPayload(GROUP:FindByName("f4_intercept"), 1 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT }, 10)
--local f4Intercept2=LAN:NewPayload(GROUP:FindByName("f4_intercept2"), 6 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT }, 15)
local f4Intercept3=LAN:NewPayload(GROUP:FindByName("f4_intercept3"), 99 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT }, 20)

local migIntercept=LAV:NewPayload(GROUP:FindByName("mig29_intercept"), 1 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT }, 99)
local migIntercept2=LAV:NewPayload(GROUP:FindByName("mig29_intercept2"), 6 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT }, 80)
local migIntercept3=LAV:NewPayload(GROUP:FindByName("mig29_intercept3"), 99 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT }, 60)

local f5Intercept=BAN:NewPayload(GROUP:FindByName("f5_intercept"), 4 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT }, 10)
local f5CAP=BAN:NewPayload(GROUP:FindByName("f5_cap"), 4 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT }, 10)

local LARf4Intercept=LAR:NewPayload(GROUP:FindByName("f4_intercept"), 1 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT }, 10)
local LARf4Intercept3=LAR:NewPayload(GROUP:FindByName("f4_intercept3"), 99 ,{AUFTRAG.Type.INTERCEPT,AUFTRAG.Type.CAP, AUFTRAG.Type.ESCORT }, 20)



--Start the Airwings
LAN:Start()
LAV:Start()
BAN:Start()
LAR:Start()
-- Create a SET of all Blue Groups

ZoneCAP = ZONE:New("CAP1") 
missionCAP=AUFTRAG:NewCAP(ZoneCAP, 10000, 450)
missionCAP:SetTime("0:01", "5:10")
capgroup=FLIGHTGROUP:New("F4_CAP")
ZoneCAP2 = ZONE:New("CAP2")
missionCAP2=AUFTRAG:NewCAP(ZoneCAP2, 10000, 450)
missionCAP2:SetTime("5:10", "6:50")
capgroup:AddMission(missionCAP)
capgroup:AddMission(missionCAP2)
-- --[[
function retry_mission()
  capgroup:AddMission(missionCAP)
  --MessageToAll('CAP is happening again')
end


function missionCAP:OnAfterSuccess(From, Event, To)
  mist.scheduleFunction(retry_mission, {}, timer.getTime() + 20, 30, timer.getTime() + 30)
end

--]]


local AgentSet=SET_GROUP:New():FilterCoalitions("red"):FilterStart()

--Declare an INTEL based off that SET, for BLUE coalition
local KGB=INTEL:New(AgentSet, coalition.side.red)  
KGB:AddAcceptZone(IranBorderZone) --give it an inclusive area  
KGB:Start() --start it

local awacs =GROUP:FindByName("EW AWACS")
local escort_awacs = AUFTRAG:NewESCORT(awacs)
LAR:AddMission(escort_awacs)
--BAN:AddMission(escort_awacs)

--THIS IS A CUSTOM FUNCTION TO CREATE A MISSION FOR ALL DETECTIONS OF THE RIGHT TYPE
function ATO(contact) 
  if contact.attribute == "Air_Bomber" then

      local grp= GROUP:FindByName(contact.groupname)
      local vec2 = grp:GetVec2()
      local coord = grp:GetCoordinate()
      local zoneR = ZONE_RADIUS:New("CapZone", vec2, 90000)
      local auftrag=AUFTRAG:NewCAP(zoneR, 20000, 500, coord, 2700, 10)
      auftrag:SetRepeatOnFailure(5)
      LAN:AddMission(auftrag)
      LAV:AddMission(auftrag)
  
  elseif contact.attribute == "Air_Fighter" then

    local grp= GROUP:FindByName(contact.groupname)
    local auftrag=AUFTRAG:NewINTERCEPT(grp)
    auftrag:SetRepeatOnFailure(5)
    LAV:AddMission(auftrag)
    LAN:AddMission(auftrag)

  else
    --MessageToAll('ignore this')
  end
end --endfunc

--- Function called when a NEW contact is detected.
function KGB:OnAfterNewContact(From, Event, To, Contact)
  local contact=Contact --Ops.Intelligence#INTEL.Contact
  ATO(contact) --fire the function for the contact  
end --end OnAfterNew

--[[
function DUB:OnAfterAssetSpawned(From, Event, To,group, asset, request)
  local escorted = group
  local escort = AUFTRAG:NewESCORT(escorted)
  RAS:AddMission(escort)

end
]]


function CAP1()
  local target=GROUP:FindByName("Vipers *Mishka*")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  BAN:AddMission(auftrag)
end

function CAP2()
  local target=GROUP:FindByName("Vipers *Bulldog*")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  BAN:AddMission(auftrag)
end

function CAP3()
  local target=GROUP:FindByName("Tomcats *Pommie*")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  BAN:AddMission(auftrag)
end

function CAP4()
  local target=GROUP:FindByName("Jeffs *Laika*")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  BAN:AddMission(auftrag)
end

function CAP5()
  local target=GROUP:FindByName("Hornets *Pluto*")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  BAN:AddMission(auftrag)
end

function CAP6()
  local target=GROUP:FindByName("Hornets *Monroe*")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  BAN:AddMission(auftrag)
end

function CAP7()
  local target=GROUP:FindByName("Hornets *Balto*")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  BAN:AddMission(auftrag)
end

function CAP8()
  local target=GROUP:FindByName("Harriers *Lassie*")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  BAN:AddMission(auftrag)
end


function CAP9()
  local target=GROUP:FindByName("Eagles *Pavlov*")
  local auftrag=AUFTRAG:NewINTERCEPT(target)
  auftrag:SetRepeatOnFailure(4)
  BAN:AddMission(auftrag)
end




Command=GROUP:FindByName("Tomcats *Pommie*")

--TOP MENU
TopMenu1 = MENU_GROUP:New(Command,"Spawn Some enemies" )
-- TopMenu3 = MENU_GROUP:New( "Auftrag Demo 17>>" )
--FIRST SUB MENU
Menu1 = MENU_GROUP_COMMAND:New(Command,"Vipers *Mishka", TopMenu1, CAP1)
Menu2 = MENU_GROUP_COMMAND:New(Command,"Vipers *Bulldog*", TopMenu1, CAP2)
Menu3 = MENU_GROUP_COMMAND:New(Command,"Tomcats *Pommie*", TopMenu1, CAP3)
Menu4 = MENU_GROUP_COMMAND:New(Command,"Jeffs *Laika*", TopMenu1, CAP4)

Menu5 = MENU_GROUP_COMMAND:New(Command,"Hornets *Pluto*", TopMenu1, CAP5)
Menu6 = MENU_GROUP_COMMAND:New(Command,"Hornets *Monroe*", TopMenu1, CAP6)
Menu7 = MENU_GROUP_COMMAND:New(Command,"Hornets *Balto*", TopMenu1, CAP7)
Menu8 = MENU_GROUP_COMMAND:New(Command,"Harriers *Lassie*", TopMenu1, CAP8)

Menu9 = MENU_GROUP_COMMAND:New(Command,"Eagles *Pavlov*", TopMenu1, CAP9)

-- mist.scheduleFunction(Timer_for_setup, {}, timer.getTime() + 90, 100, timer.getTime() + 180)